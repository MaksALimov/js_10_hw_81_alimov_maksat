import Layout from "./components/UI/Layout/Layout";
import MainPage from "./components/MainPage/MainPage";
import React from "react";

const App = () => (
    <Layout>
        <MainPage/>
    </Layout>
);

export default App;
