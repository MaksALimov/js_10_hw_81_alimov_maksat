import axios from "axios";

export const SHORT_LINKS_REQUEST = 'SHORT_LINKS_REQUEST';
export const SHORT_LINKS_SUCCESS = 'SHORT_LINKS_SUCCESS';
export const SHORT_LINKS_FAILURE = 'SHORT_LINKS_FAILURE';

export const shortLinksRequest = () => ({type: SHORT_LINKS_REQUEST});
export const shortLinksSuccess = shortedLink => ({type: SHORT_LINKS_SUCCESS, payload: shortedLink});
export const shortLinksFailure = error => ({type: SHORT_LINKS_FAILURE, payload: error});

export const shortLinks = url => {
    return async dispatch => {
        try {
            dispatch(shortLinksRequest());

            const response = await axios.post('http://localhost:8000/links', url);
            dispatch(shortLinksSuccess(response.data));
        } catch (error) {
            dispatch(shortLinksFailure(error.message));
        }
    };
};

