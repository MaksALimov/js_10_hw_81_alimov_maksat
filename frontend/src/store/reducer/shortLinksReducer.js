import {
    SHORT_LINKS_FAILURE,
    SHORT_LINKS_REQUEST,
    SHORT_LINKS_SUCCESS
} from "../actions/shortLinksActions";

const initialState = {
    loading: null,
    error: null,
    shortedLink: null,
};

const shortLinksReducer = (state = initialState, action) => {
    switch (action.type) {
        case SHORT_LINKS_REQUEST:
            return {...state, loading: true, error: false,};

        case SHORT_LINKS_SUCCESS:
            return {...state, shortedLink: action.payload, loading: false};

        case SHORT_LINKS_FAILURE:
            return {...state, loading: false, error: action.payload};

        default:
            return state;
    }
};

export default shortLinksReducer;