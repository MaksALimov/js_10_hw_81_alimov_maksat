import React from 'react';
import {Container, CssBaseline} from "@material-ui/core";

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline/>
            <Container>
                {children}
            </Container>
        </>
    );
};

export default Layout;