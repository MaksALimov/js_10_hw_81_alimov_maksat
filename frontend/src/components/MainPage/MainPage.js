import React, {useState} from 'react';
import {Button, CircularProgress, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {shortLinks} from "../../store/actions/shortLinksActions";

const useStyles = makeStyles(() => ({
    titleIndent: {
        marginTop: '100px',
        textAlign: 'center',
    },

    urlInput: {
        width: '100%',
        margin: '30px 0',
    },

    redirectContainer: {
        fontSize: '30px',
        marginTop: '50px',
        textAlign: 'center',
    },

    redirectLink: {
        textDecoration: 'none',
        borderBottom: '4px solid blue',
    },

    circularProgress: {
        margin: '30px',
    }
}));

const MainPage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [link, setLink] = useState({
        originalUrl: '',
    });

    const onInputChangeHandler = e => {
        const {name, value} = e.target;
        setLink(prevState => ({...prevState, [name]: value}));
    };

    const onSubmitFormHandler = e => {
        e.preventDefault();
        dispatch(shortLinks(link));
    };

    const loading = useSelector(state => state.shortLinks.loading);
    const error = useSelector(state => state.shortLinks.error);
    const shortedLink = useSelector(state => state.shortLinks.shortedLink);

    return (
        <div>
            <Typography
                variant="h2"
                className={classes.titleIndent}
            >
                Shorten Your Link!
            </Typography>
            <form onSubmit={onSubmitFormHandler}>
                <Grid container justifyContent="center">
                    <Grid item xs={12}>
                        <TextField
                            label="Enter your url here"
                            variant="outlined"
                            name="originalUrl"
                            value={link.originalUrl}
                            onChange={onInputChangeHandler}
                            error={!!error}
                            helperText={error}
                            className={classes.urlInput}
                        />
                    </Grid>
                    <Grid item container direction="column" alignItems="center">
                        {loading ? <CircularProgress className={classes.circularProgress}/> : null}
                    </Grid>
                    <Grid item>
                        <Button type="submit" variant="contained">Shorten!</Button>
                    </Grid>
                </Grid>
            </form>
            <Grid item>
                <Typography variant="h3">
                    <Grid
                        item xs
                        className={classes.redirectContainer}
                    >
                        <Typography variant="h4">
                            {shortedLink ? <p>Your link now looks like this:</p> : null}
                        </Typography>
                        {shortedLink ? <a
                            className={classes.redirectLink}
                            target="_blank"
                            rel="noreferrer"
                            href={`http://127.0.0.1:8000/links/${shortedLink.shortUrl}`}
                        >
                            http://127.0.0.1:8000/links/${shortedLink.shortUrl}
                        </a> : null}
                    </Grid>
                </Typography>
            </Grid>
        </div>
    );
};

export default MainPage;