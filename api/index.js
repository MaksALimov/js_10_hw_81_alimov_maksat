const express = require('express');
const app = express();
const cors = require('cors');
const links = require('./app/links');
const mongoose = require('mongoose');

app.use(cors());
app.use(express.json());
app.use('/links', links);

const port = 8000;
const run = async () => {
    await mongoose.connect('mongodb://localhost/short_links');

    app.listen(port, () => {
        console.log(`Server started ${port} port!`);
    });

    process.on('SIGINT', ()  => {
        mongoose.connection.close(() =>  {
            console.log('Disconnect');
            process.exit(0);
        });
    });
};

run().catch(e => console.error(e));