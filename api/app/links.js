const express = require('express');
const router = express.Router();
const Link = require('../models/Link');
const {nanoid} = require('nanoid');

router.get('/:shortUrl', async (req, res) => {
    try {
        const link = await Link.findOne({shortUrl: req.params.shortUrl});
        if (link) {
            res.status(301).redirect(link.originalUrl);
        } else {
            res.sendStatus(404).send({error: 'Original Url Not Found'});
        }
    } catch {
        res.sendStatus(500);
    }
});

router.post('/', async (req, res) => {
    const linkData = {
        shortUrl: nanoid(7),
        originalUrl: req.body.originalUrl,
    };

    if (!linkData.originalUrl || !linkData.shortUrl) {
        return res.sendStatus(400).send({error: 'Data not valid'});
    }

    const link = new Link(linkData);

    try {
        await link.save();
        return res.send(link);
    } catch {
        res.sendStatus(400).send({error: 'Data not valid'});
    }
});

module.exports = router;